package com.GOSecuri.Clients;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

import com.GOSecuri.dao.DaoConfigurationException;
import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.services.ComparaisonImagesService;
import com.GOSecuri.services.UtilisateurService;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.cloud.StorageClient;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;


import com.amazonaws.auth.BasicAWSCredentials;
//package com.amazonaws.services
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.rekognition.model.CompareFacesMatch;
import com.amazonaws.services.rekognition.model.CompareFacesRequest;
import com.amazonaws.services.rekognition.model.CompareFacesResult;
import com.amazonaws.services.rekognition.model.ComparedFace;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.InvalidParameterException;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.BoundingBox;
import com.amazonaws.services.rekognition.model.CompareFacesMatch;
import com.amazonaws.services.rekognition.model.CompareFacesRequest;
import com.amazonaws.services.rekognition.model.CompareFacesResult;
import com.amazonaws.services.rekognition.model.ComparedFace;
import java.util.List;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import com.amazonaws.util.IOUtils;
import java.io.ByteArrayInputStream;

public class GOSecuri {
    private static Database database = null;
    private static DaoFactory daoFactory = null;
    private static UtilisateurService utilisateurService = null;
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
      String sourceImageF = "D:\\doc\\photos\\recofaciale_test_f.jpg";
      String sourceImageT = "D:\\doc\\photos\\recofaciale_test.jpg";
      String targetImage = "D:\\doc\\photos\\recofaciale_ref.jpg";
      String txt = "D:\\doc\\doc\\Nuum_Factory\\_MSPR\\gosecuri\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\GOSecuri\\img\\test.txt";
      String txt1 = "D:\\doc\\doc\\Nuum_Factory\\_MSPR\\gosecuri\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\GOSecuri\\img\\dataPhoto.txt";
      String txt2 = "D:\\doc\\doc\\Nuum_Factory\\_MSPR\\gosecuri\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\GOSecuri\\img\\dataPhoto.png";

      //    	ComparaisonImagesService cis = new ComparaisonImagesService();
////    	cis.uploadFile(targetImage, "recofaciale_ref.jpg");
//    	try {
//    		ByteBuffer test = cis.downloadFile("recofaciale_test.jpg");
//    		ByteBuffer ref = cis.downloadFile("recofaciale_ref.jpg");
//    		cis.ComparaisonImages(test,ref);
//    	}catch (IOException e) {
//    		System.out.println(e.getMessage());
//    	}
      String everything="";
    	try(BufferedReader br = new BufferedReader(new FileReader(txt1))) {
    	    StringBuilder sb = new StringBuilder();
    	    String line = br.readLine();

    	    while (line != null) {
    	        sb.append(line);
    	        sb.append(System.lineSeparator());
    	        line = br.readLine();
    	    }
    	    everything = sb.toString();
    	} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	String base64Image= everything.split(",")[1];
//    	String base64Image=everything.substring(everything.indexOf(",") + 1);
    	System.out.println(everything.split(",")[0]);
    	System.out.println(everything.split(",").length);
    	System.out.println(everything.split(",")[1].substring(0, 50));
    	// Note preferred way of declaring an array variable
//    	byte[] underscored = "CAFEHELLO_BABEDUKE".getBytes(StandardCharsets.UTF_8);
//    	byte[] data = new BASE64Decoder().decodeBuffer(base64Image);
    	byte[] bytes,data;
		try {
			bytes = base64Image.getBytes("UTF-8");
//			String encoded = Base64.getEncoder().encodeToString(bytes);
//	    	data = Base64.getDecoder().decode(encoded);
//	    	System.out.println(data);
	    	File imgFile = new File(txt2);  
//	    	FileOutputStream output = new FileOutputStream(imgFile); 
//	    	output.write(data); 
//	    	output.flush(); output.close();
	    	data = Base64.decodeBase64(base64Image);
	    	try (OutputStream stream = new FileOutputStream(txt2)) {
	    	    stream.write(data);
	    	}
//	    	ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
//	    	BufferedImage image = ImageIO.read(bis);
//	    	bis.close();
//	    	System.out.println(image.toString());
//			ImageIO.write(image, "png", imgFile); 
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
//    	byte[] data = new Base64.getDecoder().decode(base64Image.getBytes(StandardCharsets.UTF_8));
    	
	catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}  
        
//    	cis.ComparaisonImages(sourceImageT, targetImage);
//    	cis.ComparaisonImages(sourceImageF, targetImage);
    	
    	// Imports the Google Cloud client library
//    	Float similarityThreshold = 70F;
//        String sourceImage = "D:\\doc\\photos\\recofaciale_test_f.jpg";
//        String targetImage = "D:\\doc\\photos\\recofaciale_ref.jpg";
//        ByteBuffer sourceImageBytes=null;
//        ByteyyBuffer targetImageBytes=null;
//
//        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder.defaultClient();
//
//        //Load source and target images and create input parameters
//        try (InputStream inputStream = new FileInputStream(new File(sourceImage))) {
//           sourceImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
//        }
//        catch(Exception e)
//        {
//            System.out.println("Failed to load source image " + sourceImage);
//            System.exit(1);
//        }
//        try (InputStream inputStream = new FileInputStream(new File(targetImage))) {
//            targetImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
//        }
//        catch(Exception e)
//        {
//            System.out.println("Failed to load target images: " + targetImage);
//            System.exit(1);
//        }
//
//        Image source=new Image()
//             .withBytes(sourceImageBytes);
//        Image target=new Image()
//             .withBytes(targetImageBytes);
//
//        CompareFacesRequest request = new CompareFacesRequest()
//                .withSourceImage(source)
//                .withTargetImage(target)
//                .withSimilarityThreshold(similarityThreshold);
//
//        // Call operation
//        CompareFacesResult compareFacesResult=rekognitionClient.compareFaces(request);
//
//
//        // Display results
//        List <CompareFacesMatch> faceDetails = compareFacesResult.getFaceMatches();
//        for (CompareFacesMatch match: faceDetails){
//          ComparedFace face= match.getFace();
//          BoundingBox position = face.getBoundingBox();
//          System.out.println("Face at " + position.getLeft().toString()
//                + " " + position.getTop()
//                + " matches with " + match.getSimilarity().toString()
//                + "% confidence.");
//
//        }
//        List<ComparedFace> uncompared = compareFacesResult.getUnmatchedFaces();
//
//        System.out.println("There was " + uncompared.size()
//             + " face(s) that did not match");



        try {
//        	cis.uploadFile(sourceImageT, "recofaciale_test.jpg");
//            // Création des données à rentrer
//        	Utilisateur utilisateur=new Utilisateur("RP123", "Partou", "Richy", "url1", "ndh", true);
//        	Utilisateur utilisateur2=new Utilisateur("RP124", "Assin", "Marc", "url2", "ndh2", true);
//        	Utilisateur utilisateur3=new Utilisateur("RP125", "FonFec", "Sophie", "url3", "ndh3", true);
//        	Utilisateur utilisateur4 = new Utilisateur("RP126", "Doeuf", "John", "url4", "ndh4", true);
//        	Map<String, Utilisateur> users = new HashMap<>();
//        	users.put(utilisateur.getNumeroSalarie(), utilisateur);
//        	users.put(utilisateur2.getNumeroSalarie(), utilisateur2);
//        	System.out.println(utilisateur.getNom()+utilisateur.getNumeroSalarie());
////        	System.out.println(utilisateur2.getNom()+utilisateur2.getNumeroSalarie());
////        	System.out.println(utilisateur3.getNom()+utilisateur3.getNumeroSalarie());
//        	
////        	//Injection dans la firebase sans les objets Dao
////        	
        	// Initialisation et connection
        	
        	
        	// Chargement de la clé de connection 
        	//(à générer des paramètres de firebase paramètres->Utilisateurs et autorisations->Comptes de services)
//        	FileInputStream serviceAccount =
//      			  new FileInputStream("./src/main/resources/com/GOSecuri/ServiceAccountKey.json");
//
//        	// Création des paramètres dans le connecteur
//        	FirebaseOptions options = new FirebaseOptions.Builder()
//					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
//					.setDatabaseUrl("https://mspr4-e6ecd.firebaseio.com/")
//					.setStorageBucket("mspr4-e6ecd.appspot.com")
//					.build();
//        	
//        	// Initialise
//        	FirebaseApp.initializeApp(options);
			
			
//        	FirebaseOptions options = FirebaseOptions.builder()
//        		    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//        		    .setStorageBucket("<BUCKET_NAME>.appspot.com")
//        		    .build();
//        		FirebaseApp.initializeApp(options);
//        	FirebaseStorage storage = FirebaseStorage.getInstance();
        		
////			
////	       // Création/INSERT des utilisateurs
////			//Firestore database =  FirestoreClient.getFirestore();
////			ApiFuture<WriteResult> collection = database.collection("Utilisateur").document(utilisateur2.getNumeroSalarie()).set(utilisateur2);
////			System.out.println(collection.get().getUpdateTime().toString());
////
////			//Récupération/FIND/select des utilisateurs
////			Firestore database =  FirestoreClient.getFirestore();
////			DocumentReference docRef = database.collection("Utilisateur").document("2");
////			// récupération de l'objet "Document" (type noSql)
////			ApiFuture<DocumentSnapshot> future = docRef.get();
////        	DocumentSnapshot document = future.get();
////        	// conversion de l'objet document en pojo utilisateur
////        	if(document.exists()) {
////        		utilisateur4 = document.toObject(Utilisateur.class);
////        	}
////        	System.out.println(utilisateur4.toString());
////        	
////        	//Modification/UPDATE d'un utilisateur (idem insert avec l'id déjà existant)
////        	//Firestore database =  FirestoreClient.getFirestore();
////			collection = database.collection("Utilisateur").document(utilisateur2.getNumeroSalarie()).set(utilisateur3);
////			System.out.println(collection.get().getUpdateTime().toString());
////        	
////			//DELETE suppression d'un utilisateur
////        	//Firestore database =  FirestoreClient.getFirestore();
////			collection = database.collection("Utilisateur").document(utilisateur2.getNumeroSalarie()).delete();
////        	
//        	
//        	// Injection avec Dao
//        	// On créé les objets Database, DaoFactory, ...
            database = DatabaseFactory.createDatabase();
//            String sourceImage = Thread.currentThread().getContextClassLoader().getResource("com/GOSecuri/recofaciale_ref.jpg").getPath();
//            ComparaisonImagesService cis = new ComparaisonImagesService();
////            File file = new File(sourceImage);
//            cis.uploadFile(sourceImage, "recofaciale_ref.jpg");
//            ss.uploadFile(file, "recofaciale_ref.jpg");
//            ss.download("AndreSanloreille_tocompare.png");
            //            Bucket bucket = database.getStorageBucket();
//    		Storage storage = bucket.getStorage();
//    		String nom = bucket.get("SophieFonFec_tocompare.png").getName();
//    		System.out.println(nom);
//            // Create blob
//            BlobId blobId = BlobId.of(bucket.getName(), "blob_name");
//            // Add metadata to the blob
//            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("text/plain").build();
//            // Upload blob to GCS (same as Firebase Storage)
//            Blob blob = bucket.getStorage().create(blobInfo, "Hello, Cloud Storage!".getBytes());
            int i=0;
            
//            database.getDatabase();
//            daoFactory = new DaoFactory(database);
//            utilisateurService = new UtilisateurService(daoFactory);
//            System.out.println(utilisateur2.toString());
//            utilisateur2.setMotDePasse("nounouf");;
//            utilisateurService.updateUtilisateur(utilisateur2);
//            System.out.println(utilisateurService.getUtilisateurById(utilisateur2.getNumeroSalarie()).toString());
//            utilisateurService.createUtilisateur(utilisateur4);
//            System.out.println(utilisateurService.getUtilisateurById(utilisateur4.getNumeroSalarie()).toString());
//            for(Utilisateur user : utilisateurService.getAll()) {
//            	System.out.println(user.toString());
//            }
//            utilisateurService.getAll();
            //            utilisateurService.deleteUtilisateur(utilisateur4);
//            utilisateurService.deleteUtilisateur(utilisateur4);
//        	database.getReference().child("Utilisateur").setValueAsync(users);
//        	
//        	utilisateurService.updateUtilisateur(users);
//        	System.out.println(utilisateurService.getUtilisateurById(utilisateur.getNumeroSalarie()).toString());
//        	System.out.println(utilisateurService.getUtilisateurById("othoh"));
//	          final FirebaseDatabase database = FirebaseDatabase.getInstance();
//	          DatabaseReference ref = database.getReference();
//	          DatabaseReference usersRef = ref.child("Utilisateur");
        	
//        	usersRef.setValueAsync(users);
        	
//        	System.out.println(usersRef.child("Utilisateur"));
//        	System.out.println(usersRef.toString());

        	
        	//            // On récupère la liste des groupes
//            List<Groupe> groupes = groupeService.getAll();
//
//            // Si pas de groupe alors on en crée
//            if (groupes.size() == 0) {
//
//            	for (int i = 1; i <= 10; i++ ) {
//                    Groupe groupe = new Groupe("Groupe " + i,
//                            Date.valueOf("2021-03-01"),
//                            Date.valueOf("2021-03-01"),
//                            Date.valueOf("2021-04-30"));
//
//                    // enregistre les groupe dans la bdd
//                    groupeService.createGroupe(groupe);            		
//            	}
//
//            	// récupère les nouveaux groupes créés
//                groupes = groupeService.getAll();
//            }
//
//            int idGroupe = 1;
//
//            while (idGroupe != 0) {
//
//                // Affiche les groupes
//                for (Groupe groupe : groupes) {
//                    System.out.println(groupe);
//                }
//                
//                System.out.println("Veuillez saisir un id de groupe : ");
//                idGroupe = sc.nextInt();
//
//                if (idGroupe != 0) {
//                	
//                	// récupère le groupe suivant son id pour voir si il existe
//                	Groupe groupe = groupeService.getGroupeById(idGroupe);
//                	if (groupe == null) {
//                		System.out.println(String.format("Le groupe avec l'id %d n'existe pas", idGroupe));
//                	}
//                	else {
//                		
//                    	int choix = 0;
//                    	
//                        while (choix != 3) {
//                        	List<Stagiaire> stagiaires = stagiaireService.getStagiairesFromIdGroupe(idGroupe);
//                        	
//                            if (stagiaires.size() == 0) {
//                                System.out.println("Pas de stagiaires pour ce groupe");
//                            }
//
//                            // Affiche les stagiaires
//                            for (Stagiaire stagiaire : stagiaires) {
//                                System.out.println(stagiaire);
//                            }
//
//                            System.out.println("1 : ajouter un stagiaire");
//                            System.out.println("2 : retirer un stagiaire du groupe");
//                            System.out.println("3 : retour à la liste des groupes");
//                            System.out.println("Votre choix :");
//                            choix = sc.nextInt();
//
//                            if (choix == 1) {
//                            	// récupère la liste des villes
//                            	List<Ville> villes = villeService.getAll();
//                            	
//                                // afficher la liste des villes avec le service
//                                for (Ville ville : villes) {
//                                    System.out.println(ville);
//                                }
//                                
//                                // saisir un id de ville
//                                System.out.println("Veuillez saisir un id de ville : ");
//                                int idVille = sc.nextInt();
//                                
//                                // récupère la ville suivant son id pour voir si elle existe
//                                Ville ville = villeService.getVilleById(idVille);
//                                
//                                if (ville == null) {
//                                	System.out.println(String.format("La ville avec l'id %d n'existe pas", idVille));
//                                }
//                                else {
//                                    // saisir nom, prenom
//                                    System.out.println("Veuillez saisir votre nom : ");
//                                    String nom = sc.next();
//                                    System.out.println("Veuillez saisir votre prenom : ");
//                                    String prenom = sc.next();
//                                    
//                                    // créer un objet stagiaire
//                                    Stagiaire stagiaire = new Stagiaire(nom,prenom,idVille, idGroupe);
//
//                                    // ajouter un stagiaire à la BDD avec le service
//                                    stagiaireService.createStagiaire(stagiaire);                             	
//                                }
//                            }
//                            else if (choix == 2) {
//                                // saisir un id de stagiaire
//                                System.out.println("Veuillez saisir un id du stagiaire : ");
//                                int idStagiaire = sc.nextInt();
//                                
//                                // récupère le stagiaire suivant son id 
//                                Stagiaire stagiaire = stagiaireService.getStagiaireById(idStagiaire);
//                                
//                                if (stagiaire == null) {
//                                	System.out.println(String.format("Le stagiaire avec l'id %d n'existe pas", idStagiaire));
//                                }
//                                else {
//                                    // supprimer le stagiaire
//                                	stagiaireService.deleteStagiaire(stagiaire);
//                                }
//                            }                                 
//                        }
//                	}                	
//                }
//            }
//
        }
        catch (DaoConfigurationException e) {
        	
        
            System.out.println(e.getMessage());
        }
//            catch( IOException e) {
//        	System.out.println(e.getMessage());
//        }
        	
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
