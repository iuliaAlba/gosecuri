package com.GOSecuri.dao;

import java.sql.Connection;

import java.util.Objects;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.pojos.Materiel;
import com.google.api.core.ApiFuture;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.database.DatabaseReference;

public abstract class Dao<T> {
    private Firestore connection = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    protected Database db;
    protected String tableName;
    Dao(Database db, String tableName)
    {
        this.db = db;
        this.tableName = tableName;
        
    }
    /**
     * Permet de récupérer un objet via son ID
     * @param id
     * @return obj
     */
    public abstract T find(String id);
    /**
     * Permet de récupérer une liste d'objet
     * @return List<obj>
     */
    public abstract List<T> list();
    /**
     * Permet de créer une entrée dans la base de données
     * par rapport à un objet
     * @param obj
     */
    public abstract Timestamp insert(T obj);
    /**
     * Permet de mettre à jour les données d'une entrée dans la base
     * @param obj
     */
    public abstract Timestamp update(T obj);
    /**
     * Permet la suppression d'une entrée de la base
     * @param obj
     */
    public abstract Timestamp delete(T obj);

    protected T getItemOnQuery(String tableName, String id, Class<T> clazz)
    {
        T businessObject = null;
      
        DocumentReference docRef;
        try {
        	docRef = this.db.getDatabase().collection(tableName).document(id);
	    } catch ( SQLException e ) {
	    	throw new DaoException( e );
		}
		// récupération de l'objet "Document" (type noSql)
		ApiFuture<DocumentSnapshot> future = docRef.get();
    	DocumentSnapshot document;
		try {
			document = future.get();
		} catch (InterruptedException e) {
			throw new DaoException( e );
		} catch (ExecutionException e) {
			throw new DaoException( e );
		}
    	// conversion de l'objet document en pojo utilisateur
    	if(document.exists()) {
    		businessObject = document.toObject(clazz);
    	}
        return businessObject;
    }
    protected List<T> getListOnQuery(String tableName, Class<T> clazz)
    {
        List<T> businessObjectList = new ArrayList<T>();
        CollectionReference colRef;
        try {
        	colRef = this.db.getDatabase().collection(tableName);
	    } catch ( SQLException e ) {
	    	throw new DaoException( e );
		}
		// récupération de l'objet "Document" (type noSql)
		ApiFuture<QuerySnapshot> future = colRef.get();
    	List<QueryDocumentSnapshot> documents = new ArrayList<QueryDocumentSnapshot>();;
		try {
			documents = future.get().getDocuments();
		} catch (InterruptedException e) {
			throw new DaoException( e );
		} catch (ExecutionException e) {
			throw new DaoException( e );
		}
		for(QueryDocumentSnapshot document : documents) {
          T businessObject = document.toObject(clazz);
          businessObjectList.add(businessObject);
      }
        return businessObjectList;
    }
    protected Timestamp insert( String tableName,String id, Class<T> clazz, Object objet)
    {
    	Timestamp time = Timestamp.MIN_VALUE;
    	T businessObject = null;
    	businessObject = getItemOnQuery(tableName, id, clazz);
    	if (Objects.isNull(businessObject)) {
	    	
	    	try {
				ApiFuture<WriteResult> collection = this.db.getDatabase().collection(tableName).document(id).set(objet);
				time = collection.get().getUpdateTime();
		    } catch ( SQLException e ) {
		    	throw new DaoException( e );
			}
		   catch (Exception e ) {
		    	throw new DaoException( e );
			}
    	}

    	return time;
    }
    protected Timestamp update( String tableName,String id, Class<T> clazz, Object objet)
    {

    	Timestamp time = Timestamp.MIN_VALUE;
    	T businessObject = null;
    	businessObject = getItemOnQuery(tableName, id, clazz);
    	if (Objects.nonNull(businessObject)) {
	    	try {
				ApiFuture<WriteResult> collection = this.db.getDatabase().collection(tableName).document(id).set(objet);
				time = collection.get().getUpdateTime();
		    } catch ( SQLException e ) {
		    	throw new DaoException( e );
			}
		   catch (Exception e ) {
		    	throw new DaoException( e );
			}
    	}
    	return time;
    }
    protected Timestamp delete( String tableName,String id, Class<T> clazz, Object objet)
    {
    	Timestamp time = Timestamp.MIN_VALUE;
    	try {
			ApiFuture<WriteResult> collection = this.db.getDatabase().collection(tableName).document(id).delete();
			time = collection.get().getUpdateTime();
	    } catch ( SQLException e ) {
	    	throw new DaoException( e );
		}
	   catch (Exception e ) {
	    	throw new DaoException( e );
		}
    	return time;
    }
}
