package com.GOSecuri.dao;

import com.GOSecuri.dao.Database;

public class DaoFactory {
    private Database db;

    public DaoFactory(Database db) {
        this.db = db;
    }

    public UtilisateurDao getUtilisateurDao() {
        return new UtilisateurDao(db);
    }
    public MaterielDao getMaterielDao() {
        return new MaterielDao(db);
    }
    public TransactionDao getTransactionDao() {
        return new TransactionDao(db);
    }
}
