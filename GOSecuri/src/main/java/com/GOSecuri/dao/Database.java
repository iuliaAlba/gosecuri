package com.GOSecuri.dao;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.cloud.firestore.FirestoreOptions;

public class Database {
	private GoogleCredentials credentials;
	
	
    public Database(String url,FileInputStream serviceAccount) throws IOException {
    	// Création des paramètres dans le connecteur
    	try {
    		this.credentials = GoogleCredentials.fromStream(serviceAccount);
    		FirestoreOptions optionsStore = 
	    			  FirestoreOptions.newBuilder().setTimestampsInSnapshotsEnabled(true).build();
    		FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(this.credentials)
					.setFirestoreOptions(optionsStore)
					.build();
	    	
	    	// Initialise
	    	if (FirebaseApp.getApps().isEmpty()) {
	    		FirebaseApp.initializeApp(options);
	    	}
	    	else {
	    		FirebaseApp.getInstance().delete();
	    		FirebaseApp.initializeApp(options);
    	}
	
	    	
	    } catch ( IOException e ) {
	        throw new DaoConfigurationException( "Impossible de charger les options GoogleCredentials de la clé privée de service compte et authentification " + serviceAccount, e );
	    }
    }
    /* Obtention de la base de données */
    public Firestore getDatabase() throws SQLException {
    	return FirestoreClient.getFirestore();
    }
    
}