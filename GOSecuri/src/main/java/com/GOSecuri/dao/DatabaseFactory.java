package com.GOSecuri.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DatabaseFactory {
//	private static final String PROPERTY_FILE =  System.getProperty("user.dir")+ File.separator + "src" + File.separator + "main" + File.separator + "resources"
//            + File.separator+"com"+ File.separator+"GOSecuri"+ File.separator+  "dao.properties" ;
	public static final String PROPERTY_FILE = Thread.currentThread().getContextClassLoader().getResource("com/GOSecuri/dao.properties").getPath();
	private static final String DATABASE_URL = "url";
	public static final String PROPERTY_GOOGLE_APPLICATION_CREDENTIALS = "serviceAccountKeyPath";
	
	public static Database createDatabase() throws IOException {
        Properties properties = new Properties();
        String url;
        String serviceAccountKeyPath;

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();


        InputStream fichierProperties = new FileInputStream(PROPERTY_FILE);
        

        try {
            properties.load( fichierProperties );
            url = properties.getProperty( DATABASE_URL );
            serviceAccountKeyPath = properties.getProperty( PROPERTY_GOOGLE_APPLICATION_CREDENTIALS );
            
        } catch ( IOException e ) {
            throw new DaoConfigurationException( "Impossible de charger le fichier properties " + PROPERTY_FILE, e );
        }
        
    	FileInputStream serviceAccount = new FileInputStream(classLoader.getResource(serviceAccountKeyPath).getPath());
    	
    	if ( serviceAccount == null ) throw new DaoConfigurationException( "La clé privée de service compte et authentification " + PROPERTY_GOOGLE_APPLICATION_CREDENTIALS + "  est introuvable dans le classpath.");
            
		return new Database(url,serviceAccount);
	}
}