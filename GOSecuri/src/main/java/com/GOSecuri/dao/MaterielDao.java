package com.GOSecuri.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.GOSecuri.pojos.Materiel;
import com.google.cloud.Timestamp;
import com.google.firebase.database.DatabaseReference;

public class MaterielDao extends Dao<Materiel> {
	
	DatabaseReference dbRef ;
    public static final String TABLE_NAME = "Materiel";
    
    
    MaterielDao(Database db)
    {
        super(db, TABLE_NAME);
    }

    @Override
    public Materiel find(String id) {
        
        return getItemOnQuery(tableName,id,Materiel.class);
    }

    @Override
    public List<Materiel> list() {
        String requete = String.format("SELECT * FROM %s", TABLE_NAME);
        
        return getListOnQuery(tableName,Materiel.class);
    }

    @Override
    public Timestamp insert(Materiel obj) {
        return insert(tableName,obj.getCodeMateriel() ,Materiel.class, obj);
    }

    @Override
    public Timestamp update(Materiel obj) {
    	    	
//        dbRef.child(tableName).child(obj.getCodeMateriel()).setValueAsync(obj);
        
    	return update(tableName,obj.getCodeMateriel(),Materiel.class , obj);
    }
 
    @Override
    public Timestamp delete(Materiel obj) {
    	return delete(tableName,obj.getCodeMateriel(),Materiel.class , obj);
    }
}