package com.GOSecuri.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.GOSecuri.pojos.Transaction;
import com.google.cloud.Timestamp;
import com.google.firebase.database.DatabaseReference;

public class TransactionDao extends Dao<Transaction> {
	
	DatabaseReference dbRef ;
    public static final String TABLE_NAME = "Transaction";
    
    
    TransactionDao(Database db)
    {
        super(db, TABLE_NAME);
    }

    @Override
    public Transaction find(String id) {
        
        return getItemOnQuery(tableName,id,Transaction.class);
    }

    @Override
    public List<Transaction> list() {
        
        return getListOnQuery(tableName,Transaction.class);
    }

    @Override
    public Timestamp insert(Transaction obj) {
        return insert(tableName,obj.getNumeroUtilisateur() ,Transaction.class, obj);
    }

    @Override
    public Timestamp update(Transaction obj) {
    	    	
//        dbRef.child(tableName).child(obj.getCodeTransaction()).setValueAsync(obj);
        
    	return update(tableName,obj.getNumeroUtilisateur(),Transaction.class , obj);
    }
 
    @Override
    public Timestamp delete(Transaction obj) {
    	return delete(tableName,obj.getNumeroUtilisateur(),Transaction.class , obj);
    }
}