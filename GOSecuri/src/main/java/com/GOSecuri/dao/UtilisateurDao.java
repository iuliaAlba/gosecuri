package com.GOSecuri.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.GOSecuri.pojos.Utilisateur;
import com.google.cloud.Timestamp;
import com.google.firebase.database.DatabaseReference;

public class UtilisateurDao extends Dao<Utilisateur> {
	
	DatabaseReference dbRef ;
    public static final String TABLE_NAME = "Utilisateur";
    
    
    UtilisateurDao(Database db)
    {
        super(db, TABLE_NAME);
    }

    @Override
    public Utilisateur find(String id) {
        
        return getItemOnQuery(tableName,id,Utilisateur.class);
    }

    @Override
    public List<Utilisateur> list() {
        String requete = String.format("SELECT * FROM %s", TABLE_NAME);
        
        return getListOnQuery(tableName,Utilisateur.class);
    }

    @Override
    public Timestamp insert(Utilisateur obj) {
        return insert(tableName,obj.getNumeroSalarie() ,Utilisateur.class, obj);
    }

    @Override
    public Timestamp update(Utilisateur obj) {
    	    	
//        dbRef.child(tableName).child(obj.getNumeroSalarie()).setValueAsync(obj);
        
    	return update(tableName,obj.getNumeroSalarie(),Utilisateur.class , obj);
    }
 
    @Override
    public Timestamp delete(Utilisateur obj) {
    	return delete(tableName,obj.getNumeroSalarie(),Utilisateur.class , obj);
    }
}