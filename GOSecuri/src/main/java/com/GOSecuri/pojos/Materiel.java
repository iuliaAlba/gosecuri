package com.GOSecuri.pojos;

import java.util.Date;

public class Materiel {


	private String codeMateriel;
	private String nom;
	private int quantite;
	private boolean disponible;
	
	public Materiel() {
		super();
	}



	public Materiel(String codeMateriel, String nom, int quantite, boolean disponible) {
		super();
		this.codeMateriel = codeMateriel;
		this.nom = nom;
		this.quantite = quantite;
		this.disponible = disponible;
	}


		

	@Override
	public String toString() {
		return "Materiel [codeMateriel=" + this.codeMateriel + ", nom=" + this.nom + ", quantite=" + this.quantite +  ", disponible=" + this.disponible+ "]";
	}



	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Materiel) && 
				  ((Materiel)obj).getCodeMateriel().equals(this.codeMateriel) &&
				  ((Materiel)obj).getNom().equals(this.nom) &&
				  ((Materiel)obj).getQuantite()==this.quantite &&
				  ((Materiel)obj).isDisponible()==this.disponible;
	}



	public String getCodeMateriel() {
		return codeMateriel;
	}



	public void setCodeMateriel(String codeMateriel) {
		this.codeMateriel = codeMateriel;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public int getQuantite() {
		return quantite;
	}



	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}



	public boolean isDisponible() {
		return disponible;
	}



	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
			
}
