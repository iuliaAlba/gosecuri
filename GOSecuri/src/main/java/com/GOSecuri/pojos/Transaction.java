package com.GOSecuri.pojos;

import java.util.Date;
import java.util.List;

public class Transaction {
	


	private String numeroUtilisateur;
	private Utilisateur utilisateur;
	private List<Materiel> materiels;
	private Date date;
	
	public Transaction() {
		super();
	}

	public Transaction(String  numeroUtilisateur, Utilisateur utilisateur, List<Materiel> materiels, Date date) {
		super();
		this.numeroUtilisateur =  numeroUtilisateur;
		this.utilisateur = utilisateur;
		this.materiels = materiels;
		this.date = date;
	}

	@Override
	public String toString() {
		String mat = "";
		for (Materiel tool : this.materiels)mat+=tool.toString()+"; ";
		return "Transaction [numeroUtilisateur="+this.numeroUtilisateur+", utilisateur=" + this.utilisateur.toString() + ", liste materiel= [ " + mat + ", date=" + this.date +"]";
	}

	public boolean equals(Object obj) {
		return (obj instanceof Transaction) && 
				  ((Transaction)obj).getNumeroUtilisateur().equals(this.numeroUtilisateur) &&	
				  ((Transaction)obj).getUtilisateur().equals(this.utilisateur) &&
				  ((Transaction)obj).getMateriels().containsAll(this.materiels) &&
				  ((Transaction)obj).getDate().equals(this.date);
	}


	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<Materiel> getMateriels() {
		return materiels;
	}

	public void setMateriels(List<Materiel> materiels) {
		this.materiels = materiels;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNumeroUtilisateur() {
		return numeroUtilisateur;
	}

	public void setNumeroUtilisateur(String numeroUtilisateur) {
		this.numeroUtilisateur = numeroUtilisateur;
	}


}
