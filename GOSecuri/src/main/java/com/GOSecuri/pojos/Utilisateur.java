package com.GOSecuri.pojos;

import java.util.Date;

public class Utilisateur {
	private String numeroSalarie;
	private String nom;
	private String prenom;
	private String photo;
	private String motDePasse;
	private boolean admin;
	

	public Utilisateur(String numeroSalarie, String nom, String prenom, String photo, String motDePasse, boolean admin) {
		super();
		this.numeroSalarie = numeroSalarie;
		this.nom = nom;
		this.prenom = prenom;
		this.photo = photo;
		this.motDePasse = motDePasse;
		this.admin = admin;
	}
	
	public Utilisateur() {
		super();
		this.numeroSalarie = "";
		this.nom = "";
		this.prenom = "";
		this.photo = "";
		this.motDePasse = "";
		this.admin = false;

	}

	

	@Override
	public String toString() {
		return "Member [numeroSalarie=" + numeroSalarie + ", nom=" + nom + ", prenom=" + prenom + ", photo=" + photo + ", motDePasse="
				+ motDePasse + ", admin=" + admin+ "]";
	}



	public String getNumeroSalarie() {
		return numeroSalarie;
	}



	public void setNumeroSalarie(String numeroSalarie) {
		this.numeroSalarie = numeroSalarie;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getPrenom() {
		return prenom;
	}



	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getPhoto() {
		return photo;
	}



	public void setPhoto(String photo) {
		this.photo = photo;
	}



	public String getMotDePasse() {
		return motDePasse;
	}



	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}



	public boolean isAdmin() {
		return admin;
	}



	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Utilisateur) && 
				  ((Utilisateur)obj).getNumeroSalarie().equals(numeroSalarie) &&
				  ((Utilisateur)obj).getNom().equals(nom) &&
				  ((Utilisateur)obj).getPhoto().equals(photo) &&
				  ((Utilisateur)obj).getMotDePasse().equals(motDePasse) &&
				  (((Utilisateur)obj).isAdmin() == this.admin) &&
				  ((Utilisateur)obj).getPrenom().equals(prenom);
	}
			
}
