package com.GOSecuri.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Properties;

import com.GOSecuri.dao.DaoConfigurationException;
import com.GOSecuri.dao.DatabaseFactory;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
//import com.amazonaws.services.rekognition.model.BoundingBox;
import com.amazonaws.services.rekognition.model.CompareFacesMatch;
import com.amazonaws.services.rekognition.model.CompareFacesRequest;
import com.amazonaws.services.rekognition.model.CompareFacesResult;
//import com.amazonaws.services.rekognition.model.ComparedFace;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.IOUtils;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

import java.io.OutputStream;

public class ComparaisonImagesService {

	private static final String CREDENTIALS = "awsCredentialsPath";
	private static final Float similarityThreshold = 95F;
	AmazonRekognition rekognitionClient;
	String sourceImage;
	String targetImage;
	String bucketName;
	AmazonS3 s3Client;

	public String getSourceImage() {
		return sourceImage;
	}

	public void setSourceImage(String sourceImage) {
		this.sourceImage = sourceImage;
	}

	public String getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(String targetImage) {
		this.targetImage = targetImage;
	}

	public static Float getSimilaritythreshold() {
		return similarityThreshold;
	}

	public ComparaisonImagesService() {
		super();
		Properties properties = new Properties();
		String accessKey, credentials;
		String secretKey, region;

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			InputStream fichierProperties = new FileInputStream(DatabaseFactory.PROPERTY_FILE);
			properties.load(fichierProperties);
			credentials = properties.getProperty(CREDENTIALS);
			bucketName = properties.getProperty("bucketName");
			InputStream ioCredentials = new FileInputStream(classLoader.getResource(credentials).getPath());
			if (ioCredentials == null)
				throw new DaoConfigurationException(
						"Les credentials aws " + CREDENTIALS + "  sont introuvables dans le classpath.");
			properties.load(ioCredentials);
			accessKey = properties.getProperty("aws_access_key_id");
			secretKey = properties.getProperty("aws_secret_access_key");
			region = properties.getProperty("region");

		} catch (IOException e) {
			throw new DaoConfigurationException(
					"Impossible de charger le fichier properties " + DatabaseFactory.PROPERTY_FILE, e);
		}

		s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
				.withRegion(region).build();
		rekognitionClient = AmazonRekognitionClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
				.withRegion(region).build();

	}

	public void uploadFile(String fileObjKeyName, String fileName) {
		try {
			PutObjectRequest request = new PutObjectRequest(bucketName, fileName, new File(fileObjKeyName));
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("image/jpeg");
			request.setMetadata(metadata);
			s3Client.putObject(request);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		}
	}

	public void downloadFile(String path, String fileName) throws IOException {
		S3Object fullObject = null;
		try {

			// Get an object and print its contents.
//            System.out.println("Downloading an object");
			fullObject = s3Client.getObject(new GetObjectRequest(bucketName, fileName));
//            System.out.println("Content-Type: " + fullObject.getObjectMetadata().getContentType());

			InputStream reader = new BufferedInputStream(fullObject.getObjectContent());
			File file = new File(path);
			OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));

			int read = -1;

			while ((read = reader.read()) != -1) {
				writer.write(read);
			}

			writer.flush();
			writer.close();
			reader.close();
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		} catch (IOException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		} finally {
			// To ensure that the network connection doesn't remain open, close any open
			// input streams.
			if (fullObject != null) {
				fullObject.close();
			}
		}
	}

	public ByteBuffer downloadFile(String fileName) throws IOException {
		S3Object fullObject = null;

		ByteBuffer is = null;

		try {

			// Get an object and print its contents.
//            System.out.println("Downloading an object");
			fullObject = s3Client.getObject(new GetObjectRequest(bucketName, fileName));
//            System.out.println("Content-Type: " + fullObject.getObjectMetadata().getContentType());
			InputStream reader = new BufferedInputStream(fullObject.getObjectContent());
			// Load source and target images and create input parameters
			try {
				is = ByteBuffer.wrap(IOUtils.toByteArray(reader));
			} catch (Exception e) {
				System.out.println("Failed to load source image " + sourceImage);
				System.exit(1);
			}
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		} finally {
			// To ensure that the network connection doesn't remain open, close any open
			// input streams.
			if (fullObject != null) {
				fullObject.close();
			}

		}
		return is;
	}

	public boolean ComparaisonImages(String sourceImage, String targetImage) {
		boolean same = false;
		this.sourceImage = sourceImage;
		this.targetImage = targetImage;

		ByteBuffer sourceImageBytes = null;
		ByteBuffer targetImageBytes = null;

		// Load source and target images and create input parameters
		try (InputStream inputStream = new FileInputStream(new File(sourceImage))) {
			sourceImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
		} catch (Exception e) {
			System.out.println("Failed to load source image " + sourceImage);
			System.exit(1);
		}
		try (InputStream inputStream = new FileInputStream(new File(targetImage))) {
			targetImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
		} catch (Exception e) {
			System.out.println("Failed to load target images: " + targetImage);
			System.exit(1);
		}

		Image source = new Image().withBytes(sourceImageBytes);
		Image target = new Image().withBytes(targetImageBytes);

		CompareFacesRequest request = new CompareFacesRequest().withSourceImage(source).withTargetImage(target)
				.withSimilarityThreshold(similarityThreshold);

		// Call operation
		CompareFacesResult compareFacesResult = rekognitionClient.compareFaces(request);
		// Display results
//    List <CompareFacesMatch> faceDetails = compareFacesResult.getFaceMatches();
//    for (CompareFacesMatch match: faceDetails){
//      ComparedFace face= match.getFace();
//      BoundingBox position = face.getBoundingBox();
//      System.out.println("Face at " + position.getLeft().toString()
//            + " " + position.getTop()
//            + " matches with " + match.getSimilarity().toString()
//            + "% confidence.");
//
//    }
		List<CompareFacesMatch> compared = compareFacesResult.getFaceMatches();
		if (compared.size() >= 1)
			same = true;
//    System.out.println("There was " + compared.size()
//         + " face(s) that did match");

		return same;
	}

	public boolean ComparaisonImages(ByteBuffer sourceImageBytes, ByteBuffer targetImageBytes) {
		boolean same = false;

		Image source = new Image().withBytes(sourceImageBytes);
		Image target = new Image().withBytes(targetImageBytes);

		CompareFacesRequest request = new CompareFacesRequest().withSourceImage(source).withTargetImage(target)
				.withSimilarityThreshold(similarityThreshold);

		// Call operation
		CompareFacesResult compareFacesResult = rekognitionClient.compareFaces(request);
		// Display results
//    List <CompareFacesMatch> faceDetails = compareFacesResult.getFaceMatches();
//    for (CompareFacesMatch match: faceDetails){
//      ComparedFace face= match.getFace();
//      BoundingBox position = face.getBoundingBox();
//      System.out.println("Face at " + position.getLeft().toString()
//            + " " + position.getTop()
//            + " matches with " + match.getSimilarity().toString()
//            + "% confidence.");
//
//    }
		List<CompareFacesMatch> compared = compareFacesResult.getFaceMatches();
		if (compared.size() >= 1)
			same = true;
//    System.out.println("There was " + compared.size()
//         + " face(s) that did match");

		return same;
	}

	public void delete(String keyName) {

		try {

			s3Client.deleteObject(new DeleteObjectRequest(bucketName, keyName));
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		}
	}
}
