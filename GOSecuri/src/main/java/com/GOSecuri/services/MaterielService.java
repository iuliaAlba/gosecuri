package com.GOSecuri.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.MaterielDao;
import com.GOSecuri.pojos.Materiel;

public class MaterielService {
    public MaterielService() {
		super();
	}
	private DaoFactory daoFactory;
    
    public MaterielService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    public void createMateriel(Materiel Materiel) {
    	MaterielDao materielDao = daoFactory.getMaterielDao();
    	
        materielDao.insert(Materiel);
    }
    
    public Materiel getMaterielById(String id) {
    	MaterielDao materielDao = daoFactory.getMaterielDao();
    	
        return materielDao.find(id);
    }

    public List<Materiel> getAll() {
    	MaterielDao materielDao = daoFactory.getMaterielDao();
    	
        return materielDao.list();
    }
    public List<Materiel> getAllAvailable() {
    	List<Materiel> matAv= new ArrayList<Materiel>();
    	MaterielDao materielDao = daoFactory.getMaterielDao();
    	for(Materiel tool : materielDao.list()) {
    		if(tool.isDisponible())matAv.add(tool);
    		}
        return matAv;
    }
    public void updateMateriel(Materiel obj) {
    	
    	MaterielDao materielDao = daoFactory.getMaterielDao();
    	
    	materielDao.update(obj);
    }
    public void deleteMateriel(Materiel materiel) {
    	MaterielDao materielDao = daoFactory.getMaterielDao();
    	
    	materielDao.delete(materiel);
    }

}