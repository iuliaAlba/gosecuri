package com.GOSecuri.services;

import java.util.List;
import java.util.Map;

import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.TransactionDao;
import com.GOSecuri.pojos.Transaction;

public class TransactionService {
    public TransactionService() {
		super();
	}
	private DaoFactory daoFactory;
    
    public TransactionService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    public void createTransaction(Transaction Transaction) {
    	TransactionDao transactionDao = daoFactory.getTransactionDao();
    	
        transactionDao.insert(Transaction);
    }
    
    public Transaction getTransactionById(String id) {
    	TransactionDao transactionDao = daoFactory.getTransactionDao();
    	
        return transactionDao.find(id);
    }

    public List<Transaction> getAll() {
    	TransactionDao transactionDao = daoFactory.getTransactionDao();
    	
        return transactionDao.list();
    }
    public void updateTransaction(Transaction obj) {
    	
    	TransactionDao transactionDao = daoFactory.getTransactionDao();
    	
    	transactionDao.update(obj);
    }
    public void deleteTransaction(Transaction transaction) {
    	TransactionDao transactionDao = daoFactory.getTransactionDao();
    	
    	transactionDao.delete(transaction);
    }

}