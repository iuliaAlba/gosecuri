package com.GOSecuri.services;

import java.util.List;
import java.util.Map;

import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.UtilisateurDao;
import com.GOSecuri.pojos.Utilisateur;

public class UtilisateurService {
    public UtilisateurService() {
		super();
	}
	private DaoFactory daoFactory;
    
    public UtilisateurService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    public void createUtilisateur(Utilisateur Utilisateur) {
    	UtilisateurDao utilisateurDao = daoFactory.getUtilisateurDao();
    	
        utilisateurDao.insert(Utilisateur);
    }
    
    public Utilisateur getUtilisateurById(String id) {
    	UtilisateurDao utilisateurDao = daoFactory.getUtilisateurDao();
    	
        return utilisateurDao.find(id);
    }

    public List<Utilisateur> getAll() {
    	UtilisateurDao utilisateurDao = daoFactory.getUtilisateurDao();
    	
        return utilisateurDao.list();
    }
    public void updateUtilisateur(Utilisateur obj) {
    	
    	UtilisateurDao utilisateurDao = daoFactory.getUtilisateurDao();
    	
    	utilisateurDao.update(obj);
    }
    public void deleteUtilisateur(Utilisateur utilisateur) {
    	UtilisateurDao utilisateurDao = daoFactory.getUtilisateurDao();
    	
    	utilisateurDao.delete(utilisateur);
    }

}