package com.GOSecuri.servlets;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.GOSecuri.dao.DaoConfigurationException;
import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Materiel;
import com.GOSecuri.pojos.Transaction;
import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.services.MaterielService;
import com.GOSecuri.services.TransactionService;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;

/**
 * Servlet implementation class AuthCameraServlet
 */
@WebServlet(name="AuthCameraServlet", urlPatterns = { "/authentificationCamera" })
public class AuthCameraServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PROPERTY_FILE = Thread.currentThread().getContextClassLoader().getResource("com/GOSecuri/test_photo.png").getPath();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthCameraServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		this.getServletContext().getRequestDispatcher("/authCamera.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		// Déclarations
		HttpSession session = request.getSession();
		Transaction transaction = new Transaction();
		Utilisateur user = (Utilisateur)session.getAttribute("utilisateur");
		String idTransaction = user.getNumeroSalarie();
		TransactionService ts;
		MaterielService ms;
		Database db;
		DaoFactory df;
		List<Materiel> materiels = new ArrayList<Materiel>();
		 

		

		
		

	}

}
