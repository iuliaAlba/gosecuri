package com.GOSecuri.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.GOSecuri.dao.DaoConfigurationException;
import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.services.UtilisateurService;

/**
 * Servlet implementation class AuthServlet
 */
@WebServlet(name="AuthServlet", urlPatterns = { "/authentification" })
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		this.getServletContext().getRequestDispatcher("/WEB-INF/auth.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String erreur = "";
		// récupère la session
		HttpSession session = request.getSession();
		
		String nom = request.getParameter("numeroSalarie");
		if (nom.isEmpty()) {
			erreur += "Le numéro de salarié n'est pas renseigné <br>";
		}
		
		String mdp = request.getParameter("motDePasse");
		if (mdp.isEmpty()) {
			erreur += "Le mot de passe n'est pas renseigné<br>";
		}
		
		if (!erreur.isEmpty()) {
			
			// on envoie erreur à la jsps
			request.setAttribute("erreur", erreur);
			
			// affiche la jsp
			this.getServletContext().getRequestDispatcher("/WEB-INF/auth.jsp").forward(request, response);
		}
		else {
			
			UtilisateurService us;
			Database db;
			DaoFactory df;
			Utilisateur user = null;
			try {
				
				db = DatabaseFactory.createDatabase();
				
				df = new DaoFactory(db);
				
				us = new UtilisateurService(df);
				
				// créé le membre
				user = us.getUtilisateurById(nom);
//				System.out.println(user.toString());
			} catch (DaoConfigurationException | IOException e) {
	            System.out.println(e.getMessage());
	        }
			
			if (user==null) {
				erreur="Vous n'êtes pas dans la base de données des salariés. <br>";
				request.setAttribute("erreur", erreur);
				this.getServletContext().getRequestDispatcher("/WEB-INF/auth.jsp").forward(request, response);
			}
			else if (user.getMotDePasse().compareTo(mdp)!=0) {
				erreur="Mot de passe incorrecte. <br>";
				request.setAttribute("erreur", erreur);
				this.getServletContext().getRequestDispatcher("/WEB-INF/auth.jsp").forward(request, response);
			}
			else {
				// ajoute dans la session le membre
				session.setAttribute("utilisateur", user);
				// redirige sur la page confirmation
				response.sendRedirect("Utilisateur");
			}

			
	

		}
	}

}
