package com.GOSecuri.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.util.Hashtable;
import java.lang.String;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.GOSecuri.dao.DaoConfigurationException;
import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.services.ComparaisonImagesService;
import com.GOSecuri.services.UtilisateurService;
import com.amazonaws.services.rekognition.model.InvalidParameterException;

import org.apache.commons.codec.binary.Base64;
import com.amazonaws.util.IOUtils;
import com.google.common.base.Strings;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet(name = "IndexServlet", urlPatterns = { "", "/index" })
//This annotation defines the maximum
//file size which can be taken.
@MultipartConfig(maxFileSize = 16177215)
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String IMAGES_FOLDER = "public/img";
	public static String uploadPath;
	private Hashtable<String, ByteBuffer > databasePictures = null;
	private ComparaisonImagesService cis;
	private UtilisateurService us = null;
	/*
	 * Si le dossier de sauvegarde de l'image n'existe pas, on demande sa création.
	 */
	@Override
	public void init() throws ServletException {
		uploadPath = getServletContext().getRealPath(IMAGES_FOLDER);
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists())
			uploadDir.mkdir();
		cis = new ComparaisonImagesService();
		
		Database db;
		DaoFactory df;
		
		databasePictures = new Hashtable<String, ByteBuffer>();
		try {
			
			db = DatabaseFactory.createDatabase();
			df = new DaoFactory(db);
			us = new UtilisateurService(df);
			
			for (Utilisateur u :us.getAll()) 
				if (!(Strings.isNullOrEmpty(u.getPhoto()))){
					databasePictures.put(u.getNumeroSalarie(),cis.downloadFile(u.getPhoto()));
				}
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// récupère la session
		HttpSession session = request.getSession();

		String fullPath = uploadPath + File.separator + "temporary.png";

		StringBuffer buffer = new StringBuffer();
		Reader reader = request.getReader();
		int current;
		while ((current = reader.read()) >= 0)
			buffer.append((char) current);
		String base64Image = new String(buffer);
		base64Image = base64Image.substring(base64Image.indexOf(",") + 1);
		byte[] bytes, data;
		bytes = base64Image.getBytes("UTF-8");
		data = Base64.decodeBase64(base64Image);
		try (OutputStream stream = new FileOutputStream(fullPath)) {
			stream.write(data);
		}
		 ByteBuffer sourceImageBytes=null;
	      //Load source and target images and create input parameters
	      try (InputStream inputStream = new FileInputStream(new File(fullPath))) {
	         sourceImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
	      }
	      catch(Exception e)
	      {
	          System.out.println("Failed to load source image " + fullPath);
	          System.exit(1);
	      }
	      String erreur="";

				
	      Utilisateur user = null;	
				for (String cle : databasePictures.keySet()) {

						try {
						if(cis.ComparaisonImages(sourceImageBytes, databasePictures.get(cle))) {
							user=us.getUtilisateurById(cle);
							// ajoute dans la session le membre
							session.setAttribute("utilisateur", user);
							// redirige sur la page confirmation
							response.sendRedirect("Utilisateur");
							return;
						}
						}catch(InvalidParameterException e) {
							erreur="Votre photo n'est pas dans la base de données des salariés. <br> Veuillez rentrer votre numéro de salarié et votre mot de passe. <br>";
							session.setAttribute("erreur", erreur);
							response.sendRedirect("authentification");
							return;

						}
			
				}

			if(user==null){
				erreur="Votre photo n'est pas dans la base de données des salariés. <br> Veuillez rentrer votre numéro de salarié et votre mot de passe. <br>";
				// ajoute dans la session le membre
				session.setAttribute("erreur", erreur);
				response.sendRedirect("authentification");
				return;
			}
			 
		 
	}

}
