package com.GOSecuri.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.GOSecuri.dao.DaoConfigurationException;
import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Materiel;
import com.GOSecuri.pojos.Transaction;
import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.services.ComparaisonImagesService;
import com.GOSecuri.services.MaterielService;
import com.GOSecuri.services.TransactionService;
import com.GOSecuri.services.UtilisateurService;
import com.google.common.base.Strings;

/**
 * Servlet implementation class UserPageServlet
 */
@WebServlet(name="UserPageServlet", urlPatterns = { "/Utilisateur" })
public class UserPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPageServlet() {
        super();
        // TODO Auto-generated constructor stub

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// Déclarations
		HttpSession session = request.getSession();
		Transaction transaction = new Transaction();
		Utilisateur user = (Utilisateur)session.getAttribute("utilisateur");
		String idTransaction = user.getNumeroSalarie();
		TransactionService ts;
		MaterielService ms;
		Database db;
		DaoFactory df;
		List<Materiel> materiels = new ArrayList<Materiel>();
		
		try {
			
			db = DatabaseFactory.createDatabase();
			df = new DaoFactory(db);
			ts = new TransactionService(df);
			ms = new MaterielService(df);
			
			// récupère la transaction
			transaction = ts.getTransactionById(idTransaction);
			
			// récupère la liste de matériel
			materiels = ms.getAll();
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
		if(transaction==null) {
			transaction = new Transaction(user.getNumeroSalarie() , user, new ArrayList<Materiel>(),new Date());
		}
		Hashtable<String, Boolean > isInTransaction = new Hashtable<String, Boolean>();
		for(Materiel tool : transaction.getMateriels()) {
			if (materiels.contains(tool))isInTransaction.put(tool.getCodeMateriel(), true);
			else isInTransaction.put(tool.getCodeMateriel(), true);
		}
		ComparaisonImagesService cis = new ComparaisonImagesService();
		String uploadPath = IndexServlet.uploadPath;
		String pathPhoto="public/img/"+ "logoGoSecuri.png";
		try {
			if (!(Strings.isNullOrEmpty(user.getPhoto()))) {
				pathPhoto="public/img/" + "userPicture.png";
				cis.downloadFile(uploadPath + File.separator + "userPicture.png",user.getPhoto());
				
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		request.setAttribute("photo",pathPhoto);

		request.setAttribute("isInTransaction", isInTransaction);
		request.setAttribute("materiels", materiels);
		request.setAttribute("transaction", transaction);
		this.getServletContext().getRequestDispatcher("/WEB-INF/userPage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// rÃ©cupÃ¨re la session
		HttpSession session = request.getSession();
		TransactionService ts;
		MaterielService ms ;
		Database db;
		DaoFactory df;
		Transaction transaction = new Transaction();
		List<Materiel> materiels_new = new ArrayList<Materiel>();
		
		Utilisateur user = (Utilisateur)session.getAttribute("utilisateur");
		String idTransaction = user.getNumeroSalarie();

		
		try {
			
			db = DatabaseFactory.createDatabase();
			
			df = new DaoFactory(db);
			
			ts = new TransactionService(df);
			transaction=ts.getTransactionById(idTransaction);
			ms = new MaterielService(df);
			
			List<Materiel> materiels_old =  ts.getTransactionById(user.getNumeroSalarie()).getMateriels();
			List<String> codeMatTransOld = new ArrayList<String>();
			
			for(Materiel m : materiels_old) {
				codeMatTransOld.add(m.getCodeMateriel());
			}
			
			if (request.getParameter("viderTransaction") != null) {
				int qte=0;


				
					for (Materiel tool : ms.getAll()) {
						if(codeMatTransOld.contains(tool.getCodeMateriel())){
							qte=tool.getQuantite()+1;
							tool.setQuantite(qte);
							if (qte>0)tool.setDisponible(true);
							else tool.setDisponible(false);
							ms.updateMateriel(tool);
						}
					}
					transaction.setMateriels(materiels_new);
					transaction.setDate(new Date());
					ts.updateTransaction(transaction);
					response.sendRedirect("Utilisateur");

	        } else if (request.getParameter("Transaction") != null) {
				// créé le membre
				String checkboxName = "";
				int qte=0;

//			
				for (Materiel tool : ms.getAll()) {
					
					checkboxName="checkbox"+tool.getCodeMateriel();
					
					if (request.getParameterValues(checkboxName)!=null){
						String c1=tool.getCodeMateriel();
						
						if (c1.equals(request.getParameter(checkboxName))) {
							
							if(!codeMatTransOld.contains(tool.getCodeMateriel())) {
								qte=tool.getQuantite()-1;
								tool.setQuantite(qte);
								if (qte>0)tool.setDisponible(true);
								else tool.setDisponible(false);
								ms.updateMateriel(tool);
							}
							materiels_new.add(tool);
							

						}
					}
					else if(codeMatTransOld.contains(tool.getCodeMateriel())){
						
						qte=tool.getQuantite()+1;
						tool.setQuantite(qte);
						if (qte>0)tool.setDisponible(true);
						else tool.setDisponible(false);
						ms.updateMateriel(tool);
					}
				}
				transaction.setMateriels(materiels_new);
				transaction.setDate(new Date());
				ts.updateTransaction(transaction);
				response.sendRedirect("index");
				
	        }
			
			
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
//		response.sendRedirect("");
		
				


	}
	


}
