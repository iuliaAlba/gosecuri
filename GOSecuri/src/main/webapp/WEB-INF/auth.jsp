<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>



<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>GO Securi</h1>
  <p>ESPACE AUTHENTIFICATION</p> 
</div>

<br><br><br><br><br><br>

<div class="container w3-col">
	<form method="post" action="authentification">
 		<div class="form-group">
    		<label for="login">Login:</label>
    		<input type="text" class="form-control" placeholder="Entrer votre login" id="numeroSalarie" name="numeroSalarie">
  		</div>
  		<div class="form-group">
   			<label for="pwd">Mot de passe:</label>
    		<input type="password" class="form-control" placeholder="Entrer votre mot de passe" id="motDePasse" name="motDePasse">
   		</div>
    	<br>
		<br>
		<div class="text-center">
			<button type="submit" class="btn btn-primary text-center">Valider</button>
		</div>
	</form>
  	<div id="erreur">
		<p>${erreur}</p>
	</div>
</div>


<br><br><br>

<%@include file="footer.jsp"%>
