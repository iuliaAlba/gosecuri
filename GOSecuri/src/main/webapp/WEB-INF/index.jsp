<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<script src="public/js/photo.js"></script>
<script>

var width=0;
function show() {
	var img = document.getElementById('progressGif');
    img.style.visibility = 'visible';
}

</script>
<p align="center">
<img id="progressGif" src="public/img/ajax-loader.gif" style="visibility:hidden;" />
</p>
	<div class="row">
		<div class="col-sm-8">
			<div class="contentarea" >
				<div class="camera" id="camera"> 
				    <video id="video">Video stream not available.</video>
				</div>
				<div><button id="startbutton" class="photoButton">Prendre la photo</button></div>
				<canvas id="canvas"></canvas>
			</div>
		 </div>
		 <div class="col-sm-4" >
		 	<form method="post" action="index" enctype="multipart/form-data" accept-charset=utf-8>
		 	<div id="IdButton">
		 	<button type="submit" onclick="show()" id="IdentifierBouton" disabled="disabled">S'identifier</button></div><div id="dataPhoto">
		 	</div>
		 	<div class="output">
				<img id="photo">
			</div>
			</form>  
		 </div>
	</div>


<%@include file="footer.jsp"%>