<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container w3-col">
	<div class="row" style="padding: 5% 0;">
		 <div class="col-sm-4" align="center">
		 	<c:if test="${not empty sessionScope.utilisateur}">
				Bonjour <b>${sessionScope.utilisateur.prenom} ${sessionScope.utilisateur.nom}</b>
				<fmt:formatDate value="${transaction.date}" var="formattedDate" 
                type="date" pattern="dd/MM/yyyy HH:mm:ss"/>
				<br> Dernière actualisation réalisée <br> ${formattedDate}
			</c:if>
		 </div>
		 <div class="col-sm-8" align="center">
		 <img  src="${photo}"  class="img-fluid float-right" width="150" height="auto">
		 </div>
	</div>
</div>



<div class="container w3-col">
	<!--<form class="form-inline">
		<div class="form-group form-check">
    		<label class="form-check-label">
     		 	<input class="form-check-input" type="checkbox"> Remember me
    		</label>
 		 </div>
 		 
	  <button type="submit" class="btn btn-primary mb-2">Valider</button>
	</form>
</div>-->
	<h2 id="titreMateriel">Cocher ou décocher le matériel pris</h2><br>
	<div class = "container">
  		<div class="row">
    		<div class="col-12">
    			<div id="choixMateriels">
    				<form method="post" action="Utilisateur">
     					<ul>
							<c:forEach items="${materiels}" var="item">
								<c:set var="IsInTransaction" value="${item.codeMateriel}IsInTransaction" />
								<c:choose>
									<c:when test="${isInTransaction.get(item.codeMateriel)}"> 
										<li>
											<div class="form-check">
							  					<label class="form-check-label">
							   						 <input checked="checked" type="checkbox" class="form-check-input" value="${item.codeMateriel}" id="checkbox${item.codeMateriel}" name="checkbox${item.codeMateriel}">${item.nom}.....Quantité: ${item.quantite} 
							  					</label>
											</div>
										</li>
										<br>
									</c:when>
									<c:otherwise> 
										<c:choose>
											<c:when test="${item.disponible}"> 
												<li>
													<div class="form-check">
									  					<label class="form-check-label">
									   						 <input type="checkbox" class="form-check-input" value="${item.codeMateriel}" id="checkbox${item.codeMateriel}" name="checkbox${item.codeMateriel}">${item.nom}.....Quantité: ${item.quantite} 
									  					</label>
													</div>
												</li>
												<br>
											</c:when>
											<c:otherwise> 
												<li>
													<div class="form-check">
									  					<label class="form-check-label">
									   						 <input type="checkbox" class="form-check-input" value="${item.codeMateriel}" id="checkbox${item.codeMateriel}" name="checkbox${item.codeMateriel}" disabled>${item.nom}.....Quantité: ${item.quantite}
									  					</label>
									  				</div>
									  			</li>
												<br>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose> 
							</c:forEach>	
						</ul>
						<button type="submit" id="viderTransaction" name="viderTransaction" class="btn btn-warning float-left">Vider la transaction</button>
						<button type="submit" id="Transaction" name="Transaction" class="btn btn-primary float-right">Valider</button>
					</form>	
				</div>
			</div>
		</div>

	</div>		
</div>

<%@include file="footer.jsp"%>

