<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="WEB-INF/header.jsp"%>


<div class="jumbotron text-center" style="margin-bottom:0">

		<h1>GO Securi</h1>
		<c:if test="${not empty sessionScope.utilisateur}">
		Bonjour ${sessionScope.utilisateur.prenom} ${sessionScope.utilisateur.nom} 
		</c:if>
		
		<br> Dernière actualisation : ${transaction.date}
		
	  	<p>Retirer du matériel</p> 
</div>


<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="index.jsp">Accueil</a>
  <a class="nav-link" href="adminUsers.jsp">Utilisateurs</a>
</nav>

</br>
</br>
</br>

<div class="container w3-col">
	<!--<form class="form-inline">
		<div class="form-group form-check">
    		<label class="form-check-label">
     		 	<input class="form-check-input" type="checkbox"> Remember me
    		</label>
 		 </div>
 		 
	  <button type="submit" class="btn btn-primary mb-2">Valider</button>
	</form>
</div>-->
	<h2 id="titreMateriel">Matériels</h2>
	<form method="post" action="Utilisateur">
		<div id="choixMateriels">
			<c:forEach items="${materiels}" var="item">
				<c:set var="IsInTransaction" value="${item.codeMateriel}IsInTransaction" />
				<c:choose>
					<c:when test="${isInTransaction.get(item.codeMateriel)}"> 
						<div class="form-check">
		  					<label class="form-check-label">
		   						 <input checked="checked" type="checkbox" class="form-check-input" value="${item.codeMateriel}" id="checkbox${item.codeMateriel}" name="checkbox${item.codeMateriel}">${item.nom}.....Quantité: ${item.quantite} 
		  					</label>
						</div>
					</c:when>
					<c:otherwise> 
						<c:choose>
							<c:when test="${item.disponible}"> 
								<div class="form-check">
				  					<label class="form-check-label">
				   						 <input type="checkbox" class="form-check-input" value="${item.codeMateriel}" id="checkbox${item.codeMateriel}" name="checkbox${item.codeMateriel}">${item.nom}.....Quantité: ${item.quantite} 
				  					</label>
								</div>
							</c:when>
							<c:otherwise> 
								<div class="form-check">
				  					<label class="form-check-label">
				   						 <input type="checkbox" class="form-check-input" value="${item.codeMateriel}" id="checkbox${item.codeMateriel}" name="checkbox${item.codeMateriel}" disabled>${item.nom}.....Quantité: ${item.quantite}
				  					</label>
								</div>
							</c:otherwise>
							
						</c:choose>
					</c:otherwise>
					
				</c:choose> 
				 
			</c:forEach>
		</div>
		<button type="button" id="viderTransaction" name="viderTransaction" class="btn btn-warning">Vider la transaction</button>
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
</div>

<div class="right container w3-col">
	
</div>

</br>
</br>
</br>

<%@include file="WEB-INF/footer.jsp"%>