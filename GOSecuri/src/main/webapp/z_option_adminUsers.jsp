<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="WEB-INF/header.jsp"%>


<div class="jumbotron text-center" style="margin-bottom:0">
		<h1>GO Securi</h1>
		<c:if test="${not empty sessionScope.utilisateur}">
			Bonjour ${sessionScope.utilisateur.prenom} ${sessionScope.utilisateur.nom}
		</c:if>
</div>
<br>
<br>

<h2 id="titreAdminUsers">Tableau des utilisateurs</h2>
<br>
	<form method="post" action="Utilisateur">
		<div id="afficheUtilisateur">
			<table class="table table-condensed">
				<thead>
			      <tr>
			     	<th>Photo</th>
			        <th>Nom</th>
			        <th>Prénom</th>
			        <th>Admin</th>
			        <th>Modifier</th>
			      </tr>
			    </thead>
			    <tbody>
					<c:forEach items="${utilisateurs}" var="item">
						<tr>
					        <td>${item.photo}</td>
					        <td>${item.nom}</td>
					        <td>${item.prenom}</td>
					        <td>
						        <c:set var="IsAdmin" value="true"/>
						        <c:choose>
							        <c:when test="${IsAdmin.get(item.admin)}">
							        	X
							        </c:when>
							        <c:otherwise> 
							        	-
							        </c:otherwise>
						        </c:choose>
						     </td>
						     <td>
						     	<div class="radio">
								  	<label><input type="radio" name="choixUilisteur" value="${item.numeroSalarie}"></label>
								</div>
						     </td>
				     	</tr>
					</c:forEach>
				</tbody>
			</table>
			
			
		</div>
		
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>


<%@include file="WEB-INF/footer.jsp"%>