package com.GOSecuri.dao;

import static junit.framework.TestCase.assertTrue;

import java.io.IOException;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Utilisateur;
import com.GOSecuri.services.UtilisateurService;

public class DaoFactoryTestCase {
	protected DaoFactory d;
	protected Database db;
	@Before
	public void setUp () {
		try {
			db = DatabaseFactory.createDatabase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void testCreationDaoFactory() throws Exception {
    	d = new DaoFactory(db);
    	d.getUtilisateurDao();
    	d.getMaterielDao();
    	d.getTransactionDao();
        assertTrue(true);
    }
    
}
