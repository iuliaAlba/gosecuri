package com.GOSecuri.dao;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.pojos.Utilisateur;
import com.google.cloud.firestore.Firestore;

public class DatabaseTestCase {
	protected DatabaseFactory fb;
	protected Database db;
	
	@Before
	public void setUp () {
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void connectDatabaseFromDatabaseFactory() throws Exception {
    	db = DatabaseFactory.createDatabase();
    	Firestore f = null;
    	f = db.getDatabase();
    	
        assertFalse(f.equals(null));
    }
    
}
