package com.GOSecuri.dao;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.pojos.Materiel;

public class MaterielDaoTestCase {
	protected MaterielDao uDao;
	protected Database db;
	protected List<Materiel> materiels;
	protected Materiel materiel;
	@Before
	public void setUp () {
		
		try {
			db = DatabaseFactory.createDatabase();
			
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
		uDao=new MaterielDao(db);
		materiel=new Materiel("CB06", "Chloroforme", 12, true);
		Materiel materiel1=new Materiel("FB06", "Feuille de boucher", 4, true);
		Materiel materiel2=new Materiel("MR06", "Mort aux rats", 5, false);
		Materiel materiel3=new Materiel("SF125", "Cable metalique", 3, true);
    	Materiel materiel4 = new Materiel("JD126", "Batte", 8, true);
    	materiels = new ArrayList<Materiel>();
    	materiels.add(materiel1);
    	materiels.add(materiel2);
    	materiels.add(materiel3);
    	materiels.add(materiel4);
    	for(Materiel tool : materiels) {
    		uDao.insert(tool);
    	}
//		uDao.insert(new Materiel("M15", "Mousquetons", 15, true));
//		uDao.insert(new Materiel("GDI10", "gants d’intervention", 10, true));
//		uDao.insert(new Materiel("CDST20", "ceintures de sécurité tactique", 20, true));
//		uDao.insert(new Materiel("DDM25", "détecteurs de métaux", 25, true));
//		uDao.insert(new Materiel("BDS30", "brassards de sécurité", 30, true));
//		uDao.insert(new Materiel("LT5", "lampes torches", 5, true));
//		uDao.insert(new Materiel("BAC5", "bandeaux Agents cynophiles", 5, true));
//		uDao.insert(new Materiel("GPB12", "gilets pare-balles", 12, true));
//		uDao.insert(new Materiel("CMC30", "chemises manches courtes", 30, true));
//		uDao.insert(new Materiel("B30", "blousons", 30, true));
//		uDao.insert(new Materiel("CV30", "coupe-vents", 30, true));
//		uDao.insert(new Materiel("TW20", "talkies walkies", 20, true));
//		uDao.insert(new Materiel("KO10", "kits oreillettes", 10, true));
//		uDao.insert(new Materiel("T5", "tasers", 5, true));
	}
	
	@After
	public void tearDown () {
		for(Materiel tool : materiels) {
			uDao.delete(tool);
		}
	}
	@Test
	public void testFind() throws Exception {

		assertTrue(uDao.find(materiels.get(0).getCodeMateriel()).equals(materiels.get(0)));
		assertTrue(uDao.find("tototototototototo")==null);
    }
	@Test
	public void testDelete() throws Exception {
    	uDao.insert(materiel);
    	uDao.delete(materiel);
    	assertTrue(uDao.find(materiel.getCodeMateriel())==null);
    }
	@Test
    public void testUpdate() throws Exception {
    	String newStr="ghb";
    	materiels.get(0).setNom(newStr);
    	uDao.update(materiels.get(0));
    	uDao.update(materiel);
        assertTrue(uDao.find(materiels.get(0).getCodeMateriel()).getNom().compareTo(newStr)==0);
        assertTrue(uDao.find(materiel.getCodeMateriel())==null);
    }
	@Test
    public void testInsert() throws Exception {
    	String newStr="toto";
    	materiels.get(1).setNom(newStr);
    	uDao.insert(materiels.get(1));
    	uDao.insert(materiel);
        assertFalse(uDao.find(materiels.get(1).getCodeMateriel()).getNom().compareTo(newStr)==0);
        assertTrue(materiel.equals(uDao.find(materiel.getCodeMateriel())));
        uDao.delete(materiel);
    }
	@Test
    public void testList() throws Exception {
    	List<Materiel> users = new ArrayList<Materiel>();
    	users=uDao.list();
        assertTrue(users.containsAll(materiels));
    }	
    
}
