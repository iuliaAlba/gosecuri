package com.GOSecuri.dao;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.pojos.Materiel;
import com.GOSecuri.pojos.Transaction;
import com.GOSecuri.pojos.Utilisateur;

public class TransactionDaoTestCase {
	protected TransactionDao uDao;
	protected Database db;
	protected Utilisateur utilisateur;
	protected List<Utilisateur> utilisateurs;
	protected List<Transaction> transactions;
	protected Transaction transaction;
	protected List<Materiel> materiels;
	protected Materiel materiel;
	@Before
	public void setUp () {
		
		try {
			db = DatabaseFactory.createDatabase();
			
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
		
		utilisateur=new Utilisateur("RP3", "Partou", "Richy", "url1", "ndh", true);
		Utilisateur utilisateur1=new Utilisateur("RP1", "Partou", "Richy", "url1", "ndh", true);
		Utilisateur utilisateur2=new Utilisateur("MA124", "Assin", "Marc", "url2", "ndh2", true);
		Utilisateur utilisateur3=new Utilisateur("SF125", "FonFec", "Sophie", "url3", "ndh3", true);
    	Utilisateur utilisateur4 = new Utilisateur("JD126", "Doeuf", "John", "url4", "ndh4", true);
    	utilisateurs = new ArrayList<Utilisateur>();
    	utilisateurs.add(utilisateur1);
    	utilisateurs.add(utilisateur2);
    	utilisateurs.add(utilisateur3);
    	utilisateurs.add(utilisateur4);
		
    	materiel=new Materiel("CB06", "Chloroforme", 12, true);
		Materiel materiel1=new Materiel("FB06", "Feuille de boucher", 4, true);
		Materiel materiel2=new Materiel("MR06", "Mort aux rats", 5, false);
		Materiel materiel3=new Materiel("SF125", "Cable metalique", 3, true);
    	Materiel materiel4 = new Materiel("JD126", "Batte", 8, true);
    	materiels = new ArrayList<Materiel>();
    	materiels.add(materiel1);
    	materiels.add(materiel2);
    	materiels.add(materiel3);
    	materiels.add(materiel4);
    	
		uDao=new TransactionDao(db);
		Date date = new Date();
		
		transaction=new Transaction(utilisateur.getNumeroSalarie(), utilisateur, materiels.subList(0, 2),date);
		Transaction transaction1=new Transaction(utilisateur1.getNumeroSalarie(), utilisateur1, materiels.subList(0, 2),date);
		Transaction transaction2=new Transaction(utilisateur2.getNumeroSalarie(), utilisateur2, materiels.subList(1, 3),date);
		Transaction transaction3=new Transaction(utilisateur3.getNumeroSalarie(), utilisateur3, materiels.subList(0, 3),date);
    	Transaction transaction4 = new Transaction(utilisateur4.getNumeroSalarie(), utilisateur4, materiels.subList(2, 3),date);
    	transactions = new ArrayList<Transaction>();
    	transactions.add(transaction1);
    	transactions.add(transaction2);
    	transactions.add(transaction3);
    	transactions.add(transaction4);
    	
    	for(Transaction tool : transactions) {
    		uDao.insert(tool);
    	}
//    	UtilisateurDao utilDao=new UtilisateurDao(db);
//    	List<Materiel> matVide = new ArrayList<Materiel>();
//    	for (Utilisateur user : utilDao.list()) {
//    		uDao.insert(new Transaction(user.getNumeroSalarie(),user,matVide,date));
//    	}

	}
	
	@After
	public void tearDown () {
		for(Transaction tool : transactions) {
			uDao.delete(tool);
		}
	}
	@Test
	public void testFind() throws Exception {
		assertTrue(uDao.find(transactions.get(0).getNumeroUtilisateur()).equals(transactions.get(0)));
		assertTrue(uDao.find("tototototototototo")==null);
    }
	@Test
	public void testDelete() throws Exception {
    	uDao.insert(transaction);
    	uDao.delete(transaction);
    	assertTrue(uDao.find(transaction.getNumeroUtilisateur())==null);
    }
	@Test
    public void testUpdate() throws Exception {
    	transactions.get(0).setUtilisateur(utilisateurs.get(3));
    	uDao.update(transactions.get(0));
    	uDao.update(transaction);
        assertTrue(uDao.find(transactions.get(0).getNumeroUtilisateur()).getUtilisateur().equals(utilisateurs.get(3)));
        assertTrue(uDao.find(transaction.getNumeroUtilisateur())==null);
    }
	@Test
    public void testInsert() throws Exception {
    	transactions.get(1).setUtilisateur(utilisateur);
    	uDao.insert(transactions.get(1));
    	uDao.insert(transaction);
        assertFalse(uDao.find(transactions.get(1).getNumeroUtilisateur()).getUtilisateur().equals(utilisateur));
        assertTrue(transaction.equals(uDao.find(transaction.getNumeroUtilisateur())));
        uDao.delete(transaction);
    }
	@Test
    public void testList() throws Exception {
    	List<Transaction> users = new ArrayList<Transaction>();
    	users=uDao.list();
        assertTrue(users.containsAll(transactions));
    }	
    
}
