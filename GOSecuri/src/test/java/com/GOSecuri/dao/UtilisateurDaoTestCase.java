package com.GOSecuri.dao;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.pojos.Utilisateur;

public class UtilisateurDaoTestCase {
	protected UtilisateurDao uDao;
	protected Database db;
	protected List<Utilisateur> utilisateurs;
	protected Utilisateur utilisateur;
	@Before
	public void setUp () {
		
		try {
			db = DatabaseFactory.createDatabase();
			
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
		uDao=new UtilisateurDao(db);
		utilisateur=new Utilisateur("RP3", "Partou", "Richy", "url1", "ndh", true);
		Utilisateur utilisateur1=new Utilisateur("RP1", "Partou", "Richy", "url1", "ndh", true);
		Utilisateur utilisateur2=new Utilisateur("MA124", "Assin", "Marc", "url2", "ndh2", true);
		Utilisateur utilisateur3=new Utilisateur("SF125", "FonFec", "Sophie", "url3", "ndh3", true);
    	Utilisateur utilisateur4 = new Utilisateur("JD126", "Doeuf", "John", "url4", "ndh4", true);
    	utilisateurs = new ArrayList<Utilisateur>();
    	utilisateurs.add(utilisateur1);
    	utilisateurs.add(utilisateur2);
    	utilisateurs.add(utilisateur3);
    	utilisateurs.add(utilisateur4);
    	for(Utilisateur user : utilisateurs) {
    		uDao.insert(user);
    	}
		
//    	uDao.insert(new Utilisateur("RichyPartou", "Partou", "Richy", "", "mp", true));
//    	uDao.insert(new Utilisateur("MarcAssin", "Assin", "Marc", "", "mp", true));
//    	uDao.insert(new Utilisateur("SophieFonfec", "FonFec", "Sophie", "", "mp", true));
//    	uDao.insert(new Utilisateur("JohnDoeuf", "Doeuf", "John", "", "mp", true));
//    	uDao.insert(new Utilisateur("JudaNanas", "Nanas", "Juda", "", "mp", true));
//    	uDao.insert(new Utilisateur("EmmakarenAh", "Ah", "Emma Karen", "EmmakarenAh_tocompare.png", "mp", true));
//    	uDao.insert(new Utilisateur("AgatheDeblouz", "Deblouz", "Agathe", "AgatheDeblouz_tocompare.png", "mp", true));
//    	uDao.insert(new Utilisateur("GerardMensoif", "Mensoif", "Gérard", "", "mp", true));
//    	uDao.insert(new Utilisateur("MaudeZarella", "Zarella", "Maude", "", "mp", true));
//    	uDao.insert(new Utilisateur("CecileOurkessa", "Ourkessa", "Cécile", "", "mp", true));
//    	uDao.insert(new Utilisateur("LaraLeuse", "Leuse", "Lara", "LaraLeuse_tocompare.png", "mp", true));
//    	uDao.insert(new Utilisateur("EdithAvuleur", "Avuleur", "Edith", "EdithAvuleur_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("AlainDissoir", "Dissoir", "Alain", "AlainDissoir_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("OctaveUleur", "Uleur", "Octave", "", "mp", false));
//    	uDao.insert(new Utilisateur("DebbyScott", "Scott", "Debby", "DebbyScott_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("GordonZola", "Zola", "Gordon", "", "mp", false));
//    	uDao.insert(new Utilisateur("AlonsoBistrot", "Bistrot", "Alonso", "AlonsoBistrot_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("OmerDalor", "Dalor", "Omer", "", "mp", false));
//    	uDao.insert(new Utilisateur("FannyEpi", "Epi", "Fanny", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanFilemongodet", "Filemongodet", "Jean", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanBon", "Bon", "Jean", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanPeuplu", "Peuplu", "Jean", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanAiralacascette", "Airalacascette", "Jean", "", "mp", false));
//    	uDao.insert(new Utilisateur("MarionNou", "Nou", "Marion", "", "mp", false));
//    	uDao.insert(new Utilisateur("JacquesOuche", "Ouche", "Jacques", "", "mp", false));
//    	uDao.insert(new Utilisateur("JacquesIes", "Ies", "Jacques", "", "mp", false));
//    	uDao.insert(new Utilisateur("JackUmule", "Umule", "Jack", "", "mp", false));
//    	uDao.insert(new Utilisateur("JessicaSerol", "Serol", "Jessica", "", "mp", false));
//    	uDao.insert(new Utilisateur("GuyTar", "Tar", "Guy", "", "mp", false));
//    	uDao.insert(new Utilisateur("SachaTouille", "Touille", "Sacha", "", "mp", false));
//    	uDao.insert(new Utilisateur("SachaRjelamule", "Rjelamule", "Sacha", "", "mp", false));
//    	uDao.insert(new Utilisateur("SachaGrine", "Grine", "Sacha", "", "mp", false));
//    	uDao.insert(new Utilisateur("SachaRemeu", "Remeu", "Sacha", "", "mp", false));
//    	uDao.insert(new Utilisateur("TomAte", "Ate", "Tom", "", "mp", false));
//    	uDao.insert(new Utilisateur("TomDesavoie", "De Savoie", "Tom", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanSerien", "Serien", "Jean", "", "mp", false));
//    	uDao.insert(new Utilisateur("DouglasAlavanille", "Alavanille", "Douglas", "", "mp", false));
//    	uDao.insert(new Utilisateur("YvesAtrovite", "Atrovite", "Yves", "", "mp", false));
//    	uDao.insert(new Utilisateur("YvesApleurer", "Apleurer", "Yves", "", "mp", false));
//    	uDao.insert(new Utilisateur("YvesAcraquer", "Acraquer", "Yves", "", "mp", false));
//    	uDao.insert(new Utilisateur("NordineAteur", "Ateur", "Nordine", "", "mp", false));
//    	uDao.insert(new Utilisateur("MehdiCament", "Cament", "Mehdi", "", "mp", false));
//    	uDao.insert(new Utilisateur("RobinAidochode", "Aidochode", "Robin", "", "mp", false));
//    	uDao.insert(new Utilisateur("GillesAiparbal", "Aiparbal", "Gilles", "", "mp", false));
//    	uDao.insert(new Utilisateur("BobLeponge", "Léponge", "Bob", "", "mp", false));
//    	uDao.insert(new Utilisateur("LaraTatouille", "Tatouille", "Lara", "", "mp", false));
//    	uDao.insert(new Utilisateur("LaraScasse", "Scasse", "Lara", "", "mp", false));
//    	uDao.insert(new Utilisateur("CharlesMagnes", "Magnes", "Charles", "", "mp", false));
//    	uDao.insert(new Utilisateur("MehdiTairane", "Tairané", "Mehdi", "", "mp", false));
//    	uDao.insert(new Utilisateur("LeoLandais", "Landais", "Léo", "", "mp", false));
//    	uDao.insert(new Utilisateur("TheodoreToirdefille", "Toirdéfille", "Théodore", "", "mp", false));
//    	uDao.insert(new Utilisateur("AliceToiraidrol", "Toiraidrol", "Alice", "AliceToiraidrol_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("AndreSanloreille", "Sanloreille", "André", "AndreSanloreille_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("PatAtpouri", "Atpouri", "Pat", "", "mp", false));
//    	uDao.insert(new Utilisateur("BarackAfrite", "Afrite", "Barack", "BarackAfrite_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("IrisKeurien", "Keurien", "Iris", "", "mp", false));
//    	uDao.insert(new Utilisateur("YvanToussebien", "Toussébien", "Yvan", "", "mp", false));
//    	uDao.insert(new Utilisateur("AndiValacreme", "Valacreme", "Andi", "AndiValacreme_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("AkimFatigsuila", "Fatigsuila", "Akim", "AkimFatigsuila_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("JessiePeurduprof", "Peurduprof", "Jessie", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanMeurdefroid", "Meurdefroid", "Jean", "", "mp", false));
//    	uDao.insert(new Utilisateur("JeanneUltou", "Ultou", "Jeanne", "", "mp", false));
//    	uDao.insert(new Utilisateur("AnnePadir", "Padir", "Anne", "AnnePadir_tocompare.png", "mp", false));
//    	uDao.insert(new Utilisateur("SamAitegal", "Aitégal", "Sam", "", "mp", false));
	}
	
	@After
	public void tearDown () {
		for(Utilisateur user : utilisateurs) {
			uDao.delete(user);
		}
	}
	@Test
	public void testFind() throws Exception {

		assertTrue(uDao.find(utilisateurs.get(0).getNumeroSalarie()).equals(utilisateurs.get(0)));
		assertTrue(uDao.find("tototototototototo")==null);
    }
	@Test
	public void testDelete() throws Exception {
    	uDao.insert(utilisateur);
    	uDao.delete(utilisateur);
    	assertTrue(uDao.find(utilisateur.getNumeroSalarie())==null);
    }
	@Test
    public void testUpdate() throws Exception {
    	String newStr="toto";
    	utilisateurs.get(0).setMotDePasse(newStr);
    	uDao.update(utilisateurs.get(0));
    	uDao.update(utilisateur);
        assertTrue(uDao.find(utilisateurs.get(0).getNumeroSalarie()).getMotDePasse().compareTo(newStr)==0);
        assertTrue(uDao.find(utilisateur.getNumeroSalarie())==null);
    }
	@Test
    public void testInsert() throws Exception {
    	String newStr="toto";
    	utilisateurs.get(1).setMotDePasse(newStr);
    	uDao.insert(utilisateurs.get(1));
    	uDao.insert(utilisateur);
        assertFalse(uDao.find(utilisateurs.get(1).getNumeroSalarie()).getMotDePasse().compareTo(newStr)==0);
        assertTrue(utilisateur.equals(uDao.find(utilisateur.getNumeroSalarie())));
        uDao.delete(utilisateur);
    }
	@Test
    public void testList() throws Exception {
    	List<Utilisateur> users = new ArrayList<Utilisateur>();
    	users=uDao.list();
        assertTrue(users.containsAll(utilisateurs));
    }	
    
}
