package com.GOSecuri.pojos;
import java.util.Date;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MaterielTestCase {
	protected Materiel materiel;
	
	@Before
	public void setUp () {
		materiel = new Materiel();
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyMateriel() throws Exception {
        Materiel materiel=new Materiel();

        assertTrue(true);
    }
    @Test
    public void createParameterAgent() throws Exception {
        Materiel materiel=new Materiel("CB06", "Chloroforme", 12, true);

        assertTrue(true);
    }
    @Test
    public void testToString() throws Exception {
        Materiel materiel=new Materiel("CB06", "Chloroforme", 12, true);
        String str = "Materiel [codeMateriel=" + "CB06" + ", nom=" + "Chloroforme" + ", quantite=" + 12 +  ", disponible=" + true+ "]";
        assertTrue(str.compareTo(materiel.toString())==0);
    }
    @Test
    public void testEquals() throws Exception {
    	Materiel materiel=new Materiel("CB06", "Chloroforme", 12, true);
    	Materiel materiel2=new Materiel("CB06", "Chloroforme", 12, true);
    	Materiel materiel3=new Materiel("FB06", "Feuille de boucher", 4, true);
        assertTrue(materiel.equals(materiel2));
        assertFalse(materiel.equals(materiel3));
    }
}
