package com.GOSecuri.pojos;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TransactionTestCase {
	protected Transaction transaction;
	protected List<Materiel> materiels;
	protected Utilisateur utilisateur;
	@Before
	public void setUp () {
		transaction = new Transaction();
		Materiel materiel=new Materiel("CB06", "Chloroforme", 12, true);
		Materiel materiel1=new Materiel("FB06", "Feuille de boucher", 4, true);
		Materiel materiel2=new Materiel("MR06", "Mort aux rats", 5, false);
		Materiel materiel3=new Materiel("SF125", "Cable metalique", 3, true);
    	Materiel materiel4 = new Materiel("JD126", "Batte", 8, true);
    	materiels = new ArrayList<Materiel>();
    	materiels.add(materiel1);
    	materiels.add(materiel2);
    	materiels.add(materiel3);
    	materiels.add(materiel4);
    	utilisateur=new Utilisateur("RP123", "Partou", "Richy", "url", "ndh", true);
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyTransaction() throws Exception {
        Transaction transaction=new Transaction();

        assertTrue(true);
    }
    @Test
    public void createParameterTransaction() throws Exception {
        Transaction transaction=new Transaction(utilisateur.getNumeroSalarie() , utilisateur, materiels,new Date());

        assertTrue(true);
    }
    @Test
    public void testToString() throws Exception {
    	Date date = new Date();
        Transaction transaction=new Transaction(utilisateur.getNumeroSalarie() ,utilisateur, materiels,date);
		String mat = "";
		for (Materiel tool : materiels)mat+=tool.toString()+"; ";
		String str = "Transaction [numeroUtilisateur="+utilisateur.getNumeroSalarie()+", utilisateur=" + utilisateur.toString() + ", liste materiel= [ " + mat + ", date=" + date +"]";
        assertTrue(str.compareTo(transaction.toString())==0);
    }
    @Test
    public void testEquals() throws Exception {
    	Date date = new Date();
        Transaction transaction=new Transaction(utilisateur.getNumeroSalarie() ,utilisateur, materiels,date);
        Transaction transaction2=new Transaction(utilisateur.getNumeroSalarie() ,utilisateur, materiels,date);
        Transaction transaction3=new Transaction(utilisateur.getNumeroSalarie() ,new Utilisateur("RP123", "Partou", "Richy", "url", "ndh", false), materiels,date);
        assertTrue(transaction.equals(transaction2));
        assertFalse(transaction.equals(transaction3));
    }
}
