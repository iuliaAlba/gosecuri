package com.GOSecuri.pojos;
import java.util.Date;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UtilisateurTestCase {
	protected Utilisateur utilisateur;
	
	@Before
	public void setUp () {
		utilisateur = new Utilisateur();
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyAgent() throws Exception {
        Utilisateur utilisateur=new Utilisateur();

        assertTrue(true);
    }
    @Test
    public void createParameterAgent() throws Exception {
        Utilisateur utilisateur=new Utilisateur("RP123", "Partou", "Richy", "url", "ndh", true);

        assertTrue(true);
    }
    @Test
    public void testToString() throws Exception {
        Utilisateur utilisateur=new Utilisateur("RP123", "Partou", "Richy", "url", "ndh", true);
        String str = "Member [numeroSalarie=" + "RP123" + ", nom=" + "Partou" + ", prenom=" + "Richy" + ", photo=" + "url" + ", motDePasse="
		+ "ndh" + ", admin=" + true+ "]";
        assertTrue(str.compareTo(utilisateur.toString())==0);
    }
    @Test
    public void testEquals() throws Exception {
        Utilisateur utilisateur=new Utilisateur("RP123", "Partou", "Richy", "url", "ndh", true);
        Utilisateur utilisateur2=new Utilisateur("RP123", "Partou", "Richy", "url", "ndh", true);
        Utilisateur utilisateur3=new Utilisateur("RP124", "Partou", "Richy", "url", "ndh", true);
        assertTrue(utilisateur.equals(utilisateur2));
        assertFalse(utilisateur.equals(utilisateur3));
    }
}
