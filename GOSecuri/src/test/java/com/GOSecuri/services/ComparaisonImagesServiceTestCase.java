package com.GOSecuri.services;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.dao.DaoConfigurationException;

import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Materiel;

public class ComparaisonImagesServiceTestCase {
	protected ComparaisonImagesService cis;
	protected String sourceImagePath;
	protected String targetImageTPath;
	protected String targetImageFPath;
	protected String sourceImageName;
	protected String targetImageTName;
	protected String targetImageFName;
	@Before
	public void setUp () {
		cis = new ComparaisonImagesService();
		sourceImagePath = Thread.currentThread().getContextClassLoader().getResource("com/GOSecuri/recofaciale_ref.jpg").getPath();
		targetImageTPath = Thread.currentThread().getContextClassLoader().getResource("com/GOSecuri/recofaciale_test.jpg").getPath();
		targetImageFPath = Thread.currentThread().getContextClassLoader().getResource("com/GOSecuri/recofaciale_test_f.jpg").getPath();
		sourceImageName = "recofaciale_ref.jpg";
		targetImageTName = "recofaciale_test.jpg";
		targetImageFName = "recofaciale_test_f.jpg";
		cis.uploadFile(sourceImagePath, sourceImageName);
		cis.uploadFile(targetImageFPath, targetImageFName);
		cis.uploadFile(targetImageTPath, targetImageTName);
	}
	
	@After
	public void tearDown () {
		cis.delete(sourceImageName);
		cis.delete(targetImageFName);
		cis.delete(targetImageTName);

	}
    @Test
    public void testBufferedComparaisonImagesService() throws Exception {
    	ByteBuffer source = null, targetT = null, targetF = null;
    	try {
    		source = cis.downloadFile(sourceImageName);
    		targetT = cis.downloadFile(targetImageTName);
    		targetF = cis.downloadFile(targetImageFName);
    	}catch (IOException e) {
    		System.out.println(e.getMessage());
    	}
    	assertTrue(cis.ComparaisonImages(source,targetT));
    	assertFalse(cis.ComparaisonImages(source,targetF));
    }
	@Test
	public void testComparaisonImagesService() throws Exception {
		assertFalse(cis.ComparaisonImages(sourceImagePath,targetImageFPath));
		assertTrue(cis.ComparaisonImages(sourceImagePath,targetImageTPath));
    }
}
