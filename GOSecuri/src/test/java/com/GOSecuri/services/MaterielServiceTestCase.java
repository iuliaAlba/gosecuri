package com.GOSecuri.services;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.dao.DaoConfigurationException;

import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Materiel;

public class MaterielServiceTestCase {
	protected MaterielService us;
	protected Database db;
	protected DaoFactory df;
	protected List<Materiel> materiels;
	protected Materiel materiel;
	@Before
	public void setUp () {
		try {
			db = DatabaseFactory.createDatabase();	
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
		df = new DaoFactory(db);
		us = new MaterielService(df);
		materiel=new Materiel("CB06", "Chloroforme", 12, true);
		Materiel materiel1=new Materiel("FB06", "Feuille de boucher", 4, true);
		Materiel materiel2=new Materiel("MR06", "Mort aux rats", 5, false);
		Materiel materiel3=new Materiel("SF125", "Cable metalique", 3, true);
    	Materiel materiel4 = new Materiel("JD126", "Batte", 8, false);
    	materiels = new ArrayList<Materiel>();
    	materiels.add(materiel1);
    	materiels.add(materiel2);
    	materiels.add(materiel3);
    	materiels.add(materiel4);
    	for(Materiel tool : materiels) {
    		us.createMateriel(tool);
    	}
	}
	
	@After
	public void tearDown () {
		for(Materiel tool : materiels) {
			us.deleteMateriel(tool);
		}
	}
    @Test
    public void testGetMaterielById() throws Exception {
		assertTrue(us.getMaterielById(materiels.get(0).getCodeMateriel()).equals(materiels.get(0)));
		assertTrue(us.getMaterielById("tototototototototo")==null);
    }
	@Test
	public void testMateriel() throws Exception {
    	us.createMateriel(materiel);
    	us.deleteMateriel(materiel);
    	assertTrue(us.getMaterielById(materiel.getCodeMateriel())==null);
    }
	
	@Test
    public void testUpdateMateriel() throws Exception {
    	String newStr="ghb";
    	materiels.get(0).setNom(newStr);
    	us.updateMateriel(materiels.get(0));
    	us.updateMateriel(materiel);
        assertTrue(us.getMaterielById(materiels.get(0).getCodeMateriel()).getNom().compareTo(newStr)==0);
        assertTrue(us.getMaterielById(materiel.getCodeMateriel())==null);
    }
	@Test
    public void testCreateMateriel() throws Exception {
    	String newStr="toto";
    	materiels.get(1).setNom(newStr);
    	us.createMateriel(materiels.get(1));
    	us.createMateriel(materiel);
        assertFalse(us.getMaterielById(materiels.get(1).getCodeMateriel()).getNom().compareTo(newStr)==0);
        assertTrue(materiel.equals(us.getMaterielById(materiel.getCodeMateriel())));
        us.deleteMateriel(materiel);
    }
	@Test
    public void testList() throws Exception {
    	List<Materiel> users = new ArrayList<Materiel>();
    	users=us.getAll();
        assertTrue(users.containsAll(materiels));
    }	
	@Test
    public void testAvailableList() throws Exception {
		int avMat=0;
    	for(Materiel tool : us.getAll()) {
    		if(tool.isDisponible())avMat++;
    	}
        assertEquals(us.getAllAvailable().size(),avMat);
    }
}
