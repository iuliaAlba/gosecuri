package com.GOSecuri.services;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.GOSecuri.dao.DaoConfigurationException;

import com.GOSecuri.dao.DaoFactory;
import com.GOSecuri.dao.Database;
import com.GOSecuri.dao.DatabaseFactory;
import com.GOSecuri.pojos.Utilisateur;

public class UtilisateurServiceTestCase {
	protected UtilisateurService us;
	protected Database db;
	protected DaoFactory df;
	protected List<Utilisateur> utilisateurs;
	protected Utilisateur utilisateur;
	@Before
	public void setUp () {
		try {
			db = DatabaseFactory.createDatabase();	
		} catch (DaoConfigurationException | IOException e) {
            System.out.println(e.getMessage());
        }
		df = new DaoFactory(db);
		us = new UtilisateurService(df);
		utilisateur=new Utilisateur("RP3", "Partou", "Richy", "url1", "ndh", true);
		Utilisateur utilisateur1=new Utilisateur("RP1", "Partou", "Richy", "url1", "ndh", true);
		Utilisateur utilisateur2=new Utilisateur("MA124", "Assin", "Marc", "url2", "ndh2", true);
		Utilisateur utilisateur3=new Utilisateur("SF125", "FonFec", "Sophie", "url3", "ndh3", true);
    	Utilisateur utilisateur4 = new Utilisateur("JD126", "Doeuf", "John", "url4", "ndh4", true);
    	utilisateurs = new ArrayList<Utilisateur>();
    	utilisateurs.add(utilisateur1);
    	utilisateurs.add(utilisateur2);
    	utilisateurs.add(utilisateur3);
    	utilisateurs.add(utilisateur4);
    	for(Utilisateur user : utilisateurs) {
    		us.createUtilisateur(user);
    	}
	}
	
	@After
	public void tearDown () {
		for(Utilisateur user : utilisateurs) {
			us.deleteUtilisateur(user);
		}
	}
    @Test
    public void testGetUtilisateurById() throws Exception {
		assertTrue(us.getUtilisateurById(utilisateurs.get(0).getNumeroSalarie()).equals(utilisateurs.get(0)));
		assertTrue(us.getUtilisateurById("tototototototototo")==null);
    }
	@Test
	public void testUtilisateur() throws Exception {
    	us.createUtilisateur(utilisateur);
    	us.deleteUtilisateur(utilisateur);
    	assertTrue(us.getUtilisateurById(utilisateur.getNumeroSalarie())==null);
    }
	
	@Test
    public void testUpdateUtilisateur() throws Exception {
    	String newStr="toto";
    	utilisateurs.get(0).setMotDePasse(newStr);
    	us.updateUtilisateur(utilisateurs.get(0));
    	us.updateUtilisateur(utilisateur);
        assertTrue(us.getUtilisateurById(utilisateurs.get(0).getNumeroSalarie()).getMotDePasse().compareTo(newStr)==0);
        assertTrue(us.getUtilisateurById(utilisateur.getNumeroSalarie())==null);
    }
	@Test
    public void testCreateUtilisateur() throws Exception {
    	String newStr="toto";
    	utilisateurs.get(1).setMotDePasse(newStr);
    	us.createUtilisateur(utilisateurs.get(1));
    	us.createUtilisateur(utilisateur);
        assertFalse(us.getUtilisateurById(utilisateurs.get(1).getNumeroSalarie()).getMotDePasse().compareTo(newStr)==0);
        assertTrue(utilisateur.equals(us.getUtilisateurById(utilisateur.getNumeroSalarie())));
        us.deleteUtilisateur(utilisateur);
    }
	@Test
    public void testList() throws Exception {
    	List<Utilisateur> users = new ArrayList<Utilisateur>();
    	users=us.getAll();
        assertTrue(users.containsAll(utilisateurs));
    }	
}
